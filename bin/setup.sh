#!/bin/bash

if hostname | grep docker1
then
  # Create Docker Registry and configure
  if ! grep "insecure" /usr/lib/systemd/system/docker.service
  then
    sed -i "s/ExecStart=\/usr\/bin\/dockerd.*/ExecStart=\/usr\/bin\/dockerd --insecure-registry 192.168.0.200:5000/" /usr/lib/systemd/system/docker.service
    systemctl daemon-reload
    systemctl restart docker
  fi

  if docker ps -a 2>&1 >/dev/null
  then
    docker rm -f $(docker ps -a -q)
  fi

  # Run Docker registry
  if ! docker ps | grep registry
  then
    docker run --name dockerreg -d -p5000:5000 -v /vagrant/files/dockerreg:/var/lib/registry registry
  fi

  # Build onecmdb, wildfly images
  docker build -t onecmdb /vagrant/files/onecmdb/
  docker build -t wildfly /vagrant/files/wildfly/

  # Tag em
  docker tag onecmdb 192.168.0.200:5000/onecmdb
  docker tag wildfly 192.168.0.200:5000/wildfly
  # Push em
  docker push 192.168.0.200:5000/onecmdb
  docker push 192.168.0.200:5000/wildfly

  # Pull only onecmdb and wildfly for VM 1
  docker pull 192.168.0.200:5000/onecmdb
  docker pull 192.168.0.200:5000/wildfly

  # Run wildfly and onecmdb
  docker run --name=onecmdb -P -d 192.168.0.200:5000/onecmdb
  docker run --name=wildfly -P -d -v /wildfly/deployments:/opt/wildfly/wildfly-10.1.0.Final/standalone/deployments 192.168.0.200:5000/wildfly

  # Run Consul Agent listening for consul server
  docker run -d --net=host -e 'CONSUL_LOCAL_CONFIG={"skip_leave_on_interrupt": true}' consul agent -bind=192.168.0.200 -retry-join=192.168.0.202 -node=felix

  # Run Docker Registrator
  docker run -d --name=registrator --net=host --volume=/var/run/docker.sock:/tmp/docker.sock gliderlabs/registrator:latest consul://192.168.0.202:8500

fi

if hostname | grep docker2
then

  if docker ps -a 2>&1 >/dev/null
  then
    docker rm -f $(docker ps -a -q)
  fi

  # Build haproxy-consul images
  docker build -t haproxy-consul /vagrant/files/haproxy-consul/

  # Tag em
  docker tag onecmdb 192.168.0.201:5000/onecmdb
  docker tag wildfly 192.168.0.201:5000/wildfly
  # Push em
  docker push 192.168.0.201:5000/onecmdb
  docker push 192.168.0.201:5000/wildfly

  # Pull only onecmdb and wildfly for VM 2
  docker pull 192.168.0.201:5000/onecmdb
  docker pull 192.168.0.201:5000/wildfly

  # Run wildfly and onecmdb
  docker run --name=onecmdb -P -d 192.168.0.201:5000/onecmdb
  docker run --name=wildfly -P -d -v /wildfly/deployments:/opt/wildfly/wildfly-10.1.0.Final/standalone/deployments 192.168.0.201:5000/wildfly

  # Run Consul Agent listening for consul server
  docker run -d --net=host -e 'CONSUL_LOCAL_CONFIG={"skip_leave_on_interrupt": true}' consul agent -bind=192.168.0.201 -retry-join=192.168.0.202 -node=wahid

  # Run Docker Registrator
  docker run -d --name=registrator --net=host --volume=/var/run/docker.sock:/tmp/docker.sock gliderlabs/registrator:latest consul://192.168.0.202:8500

fi

if hostname | grep docker3
then

  if docker ps -a 2>&1 >/dev/null
  then
    docker rm -f $(docker ps -a -q)
  fi

  docker run --net=host -d -e 'CONSUL_LOCAL_CONFIG={"skip_leave_on_interrupt": true}' consul agent -server -bind=192.168.0.202 -node=server -advertise=192.168.0.202 -client=192.168.10.202 -bootstrap


  # ZABBIX
  docker run -d -P --name zabbix  berngp/docker-zabbix

  # JENKINS (username: user    password: secret)
  cp -r /vagrant/files/jenkins /home/vagrant/jenkins
  docker run -d -u root --name=jenkins -p 8080:8080 -p 50000:50000 -v /home/vagrant/jenkins:/var/jenkins_home jenkins

  # Build haproxy-consul images
  docker build -t haproxy-consul /vagrant/files/haproxy-consul
  # Tag em
  docker tag haproxy-consul 192.168.0.202:5000/haproxy-consul
  # Push em
  docker push 192.168.0.202:5000/haproxy-consul

  # Pull only haproxy-consul for VM 3
  docker pull 192.168.0.202:5000/haproxy-consul

  # Run haproxy-consul
  docker run -d -P --name=haproxy-consul -v /home/vagrant/haproxy-consul:/templates/haproxy-consul 192.168.0.202:5000/haproxy-consul

  # ZABBIX
  docker run -d -P --name zabbix  berngp/docker-zabbix

  # JENKINS (username: user    password: secret)
  cp -r /vagrant/files/jenkins /home/vagrant/jenkins
  docker run -d -u root --name=jenkins -p 8080:8080 -p 50000:50000 -v /home/vagrant/jenkins:/var/jenkins_home jenkins

  # Run Consul Server
  docker run -d --net=host -e 'CONSUL_LOCAL_CONFIG={"skip_leave_on_interrupt": true}' consul agent -server -bind=192.168.0.202 -node=seb -advertise=192.168.0.202 -client=192.168.0.202 -bootstrap

  # Run Docker Registrator
  docker run -d --name=registrator --net=host --volume=/var/run/docker.sock:/tmp/docker.sock gliderlabs/registrator:latest consul://192.168.0.202:8500

  # Run MySQL Database
  docker run -d --name test-mariadb -e MYSQL_ROOT_PASSWORD=secret mariadb
  docker exec -it test-mariadb mysql -psecret -e "create database onecmdb"

fi
